<!--
var shiftEnabled = 1;
var shiftAmountRemaining = 0;
var shiftAmountPerIteration = 0;
var shiftDirection = 0;
var outComponent = 0;
var inComponent = 0;
var timeoutID1 = 0;
var timeoutID2 = 0;
var MMCvRow = 0;
var MMCvOuterContainer = 0;
var MMCvTitleObject = 0;
var MMCvRefHovsObject = 0;
var MMCvTabsObject = 0;
var MMCvSelectorObject = 0;
var MMCvViewerObject = 0;
var MMCvViewBackingObject = 0;
var MMCvLABEL = 0;
var MMCvCAPTION = 1;
var MMCvIMAGE = 2;
var MMCvDATA_CNT = 3;
var currentMMCtype = -1;
var currentMMCindex = 0;
var MMCvCSelector = '';
var MMCvCViewerStart = '';
var MMCvCViewerEnd = '';
var MMCvSelectorHTML = new Array();
 MMCvSelectorHTML[MMCvVIDEO] = "";
 MMCvSelectorHTML[MMCvAUDIO] = "";
 MMCvSelectorHTML[MMCvOTHER] = "";
var MMCvSelectorLists = new Array();
 MMCvSelectorLists[MMCvVIDEO] = null;
 MMCvSelectorLists[MMCvAUDIO] = null;
 MMCvSelectorLists[MMCvOTHER] = null;
var MMCvSelectorItems = new Array();
 MMCvSelectorItems[MMCvVIDEO] = null;
 MMCvSelectorItems[MMCvAUDIO] = null;
 MMCvSelectorItems[MMCvOTHER] = null;
var MMCvSelectorCollapseItems = new Array();
 MMCvSelectorCollapseItems[MMCvVIDEO] = null;
 MMCvSelectorCollapseItems[MMCvAUDIO] = null;
 MMCvSelectorCollapseItems[MMCvOTHER] = null;
var MMCvSelectorPrevInactive = null;
var MMCvSelectorPrevActive = null;
var MMCvSelectorNextInactive = null;
var MMCvSelectorNextActive = null;
var audioPlayerWidth = 318;
var audioPlayerHeight = 29;
var videoPlayerWidth = 320;
var videoPlayerHeight = 266;
var reqFlashVersion = "9.0.0";
var videoNoFlashWidth = 318;
var videoNoFlashHeight = 260;

// MMCv Selector Indexing notes... watch indexing (0..N-1) components vs (1..N) selectors
var gapLSelectorStart = -1;
var gapLSelectorEnd = -1;
var gapRSelectorStart = -1;
var gapRSelectorEnd = -1;
var MMCvSelectorMidWidth = 8;
var MMCinitialized = 0;
var MMCvSizerHTML;
MMCvSizerHTML = '<div style="overflow: hidden; width: 0px; height: 0px;"><div class="MMCvCaption" id="MMCvSizer1">1</div></div>';
var MMCvSizer1 = null;
var MMCvHeight1Prev = 0;
var MMCvHeight1Line = 0;
var MMCvImageMaxHeight = 50;
var MMCvImageMaxWidth = 50;
var MMCvVideoImageMaxHeight = 165; 
var MMCvVideoImageMaxWidth = 320; 
var MMCvHeightImageMax = MMCvImageMaxHeight + 15; // pad
var MMCvVideoCaptionHeight = 36;
var MMCvVideoCaptionHeightMax = MMCvVideoCaptionHeight + 15; //pad
var MMCvCaptionImageReplaceHeight = 30;
var MMCvCaptionImageReplaceWidth = 100;
var MMCvHeightLessPhrase = 0;
var MMCvHeightMorePhrase = 0;
var MMCvHeightLessCaption = 0;
var MMCvHeightStandardCaption = 0;
var MMCvHeightComponentWrapper = 0;
var MMCvWidthComponent = 340;
var MMCvuniqueAnchorIndex = 0;
var currentSourceAnchorName = "";
var MMCvTabHidden = 0;

MMCvImageDimensions.prototype.getDimensions = function(inImage)
{
 this.width = 0;
 this.height = 0;
 if (inImage.naturalWidth)
 { this.width = inImage.naturalWidth;
   this.height = inImage.naturalHeight;
 }
 else 
 { //_Save Styling
  var saveClassName = inImage.className;
  var saveStyleWidth = inImage.style.width;
  var saveStyleHeight = inImage.style.height;
  var saveStyleBorderWidth = inImage.style.borderWidth;
  var saveStylepadding = inImage.style.padding;
  var saveAttrWidth = inImage.getAttribute('width');
  var saveAttrHeight = inImage.getAttribute('height');
  //_Remove styling
  inImage.className = '';
  inImage.style.width = 'auto';
  inImage.style.height = 'auto';
  inImage.style.borderWidth = '0';
  inImage.style.padding = '0';
  inImage.removeAttribute('width');
  inImage.removeAttribute('height');
  //_Get real dims 
  this.width = parseInt(inImage.width);
  this.height = parseInt(inImage.height);
  //_Reset styling
  inImage.setAttribute('width', saveAttrWidth );
  inImage.setAttribute('height', saveAttrHeight );
  inImage.style.width = saveStyleWidth;
  inImage.style.height = saveStyleHeight;
  inImage.style.padding = saveStylepadding;
  inImage.style.borderWidth = saveStyleBorderWidth;
  inImage.className = saveClassName;
 }
}

function MMCvHide()
{
 if ((null != MMCvRow) && (!MMCvTabHidden) && (MMCinitialized))
 {
  MMCvTitleObject.style.display = "none";
  MMCvRefHovsObject.style.display = "none";
  MMCvTabsObject.style.display = "none";
  MMCvSelectorObject.style.display = "none";
  MMCvViewerObject.style.display = "none";
  MMCvViewBackingObject.style.display = "none";
  MMCvOuterContainer.style.display = "none";
  MMCvTabHidden = 1;
 }
}

function MMCvShow()
{
 if ((null != MMCvRow) && (MMCvTabHidden) && (MMCinitialized))
 {
  MMCvOuterContainer.style.display = "block";
  MMCvTitleObject.style.display = "block";
  MMCvRefHovsObject.style.display = "block";
  MMCvTabsObject.style.display = "block";
  MMCvSelectorObject.style.display = "block";
  MMCvViewBackingObject.style.display = "block";
  MMCvViewerObject.style.display = "block";
  MMCvTabHidden = 0;
 }
}

function MMCvSelectorList(typeID, inNode, inIndex)
{
 if ("undefined" == inIndex)
 { inIndex = 0; }
 this.myID = typeID;
 this.MMCvNode = inNode;
 this.currentIndex = inIndex;
}

MMCvComponent.prototype.showSelf = function()
{ 
 if (null != this.MMCvNode)
 { this.MMCvNode.style.left = 0; }
 else
 { this.initialize(1); }
 this.resetCaptionHeight();
}

MMCvComponent.prototype.hideSelf = function()
{
 if (null != this.MMCvNode)
 { this.MMCvNode.style.left = MMCvWidthComponent; }
}

MMCvComponent.prototype.initialize = function(showSelf)
{
 var sourceAnchors;
 var newNode;
 var newAttribute;
 if ("undefined" == showSelf)
 { showSelf = 0; }
 if (null == this.MMCvNode)
 {
  if (this.myID.indexOf("VIDEO") > -1)
  { this.myType = MMCvVIDEO; }
  else if (this.myID.indexOf("AUDIO") > -1)
  { this.myType = MMCvAUDIO; }
  else
  { this.myType = MMCvOTHER; }
  this.sourceNode = document.getElementById(this.myID).parentNode;
  if (null != this.sourceNode)
  {
   sourceAnchors = getElementsByClassName(this.sourceNode, 'A', 'MMCvANCHOR_SRC');
   if (0 == sourceAnchors.length)
   {
    newNode = document.createElement('a');
    if (null != newNode)
    {
     MMCvuniqueAnchorIndex++;
     var anchorName = "MMCvA_" + MMCvuniqueAnchorIndex;
     newAttribute = document.createAttribute("name");
     if  (null != newAttribute)
     {
      newAttribute.nodeValue = anchorName;
      newNode.setAttributeNode(newAttribute);
      newAttribute = null;
     }
     newNode.id = anchorName;
     newNode.className = 'MMCvANCHOR_SRC';
     this.sourceNode.insertBefore(newNode, this.sourceNode.firstChild);
     newNode = null;
    }
   }
   this.MMCvNode = this.getMyNode();
   if (this.MMCvNode)
   {
    MMCvViewerObject.insertBefore(this.MMCvNode, MMCvViewerObject.firstChild);
    if(!isAAI2Disabled && this.MMCvFlashVars){
     this.embedPlayerWidget();
    }
    if (MMCvHeightStandardCaption > 0)
    { this.resetCaptionHeight(); }
    if (showSelf)
    { this.MMCvNode.style.left = 0; }
   }
  }
 }
}

MMCvComponent.prototype.repositionInnerRefHovs = function (inNode)
{
 var j;
 var refPreviewNode;
 for (j=0; j < inNode.childNodes.length;)
 {
  if (inNode.childNodes[j].nodeType == 1)
  { // Only work on ELEMENT_NODE's
   if (inNode.childNodes[j].className.indexOf("refPreview") >= 0)
   {
    refPreviewNode = inNode.removeChild(inNode.childNodes[j]);
    MMCvRefHovsObject.appendChild(refPreviewNode);
   }
   else
   { this.repositionInnerRefHovs(inNode.childNodes[j]); j++; }
  }
  else
  { j++; }
 }
}

MMCvComponent.prototype.augmentMMCvElementHierarchy = function (inNode)
{
 if (inNode.nodeType != 1) // ELEMENT node
 { return; }
 var prependString = "MMCvUn";
 var idValue = inNode.id;
 var nameValue = inNode.getAttribute("name");
 var index;
 var oldTempContainerNode;
 var newTempContainerNode;
 var replacementNode;
 var innerMMCvRefLinkHTML;
 var inNodeToBeReplaced = 0;
 var replaceImgTblNode = 0;
 var newInnerMMCvRefLinkHTML = "";
 var j;
 if ((null != idValue) && ("" != idValue) && ("undefined" != idValue))
 { inNode.id = prependString + idValue; }
 if ((null != nameValue) && ("" != nameValue) && ("undefined" != nameValue))
 {
  nameValue = prependString + nameValue;
  inNode.removeAttribute('name');
  inNode.setAttribute("name", nameValue);
 }
 if ("A" == inNode.tagName)
 {
  if (inNode.className.indexOf("MMCvREFLINK_SRC") >= 0)
  {
   oldTempContainerNode = document.createElement('div');
   if (null != oldTempContainerNode)
   {
    inNode.parentNode.replaceChild(oldTempContainerNode, inNode);
    oldTempContainerNode.appendChild(inNode);
    innerMMCvRefLinkHTML = oldTempContainerNode.innerHTML;
    index = innerMMCvRefLinkHTML.indexOf("RefPreview.showRef(");
    if (index >= 0)
    {  
     index += 19;  // Length of RefPreview... string
     index = innerMMCvRefLinkHTML.indexOf(",", index);  // Skip parm 1
     if (index >= 0)
     {
      index += 1; 
      index = innerMMCvRefLinkHTML.indexOf(",", index);  // Skip parm 2
      if (index >= 0)
      {
       index += 1;
       while ((' ' == innerMMCvRefLinkHTML.charAt(index))
              || ('	' == innerMMCvRefLinkHTML.charAt(index)))
       { index++; }
       if  (('"' == innerMMCvRefLinkHTML.charAt(index))
            || ("'" == innerMMCvRefLinkHTML.charAt(index)))
       { index++; } // opening quote
       // found insertionPoint
       newInnerMMCvRefLinkHTML  = innerMMCvRefLinkHTML.substr(0,index);
       newInnerMMCvRefLinkHTML += prependString;
       newInnerMMCvRefLinkHTML += innerMMCvRefLinkHTML.substr(index);
       newTempContainerNode = document.createElement('div');
       newTempContainerNode.innerHTML = newInnerMMCvRefLinkHTML;
       replacementNode = newTempContainerNode.removeChild(
                                          newTempContainerNode.firstChild);
       // newTempContainerNode is now garbage
       inNodeToBeReplaced = 1;
      }
     }
    }
    if (!inNodeToBeReplaced)
    { replacementNode = oldTempContainerNode.removeChild(inNode); }
    oldTempContainerNode.parentNode.replaceChild(replacementNode, 
                                                 oldTempContainerNode);
    inNode = replacementNode;
   }
  }
 }
 else if ("IMG" == inNode.tagName)
 {
  var tNode = inNode.cloneNode(0);
  var tHideDiv = document.createElement('div');
  tHideDiv.style.width = "0";
  tHideDiv.style.height = "0";
  tHideDiv.style.overflow = "hidden";
  tHideDiv.appendChild(tNode);
  MMCvViewerObject.appendChild(tHideDiv);
  var imageDimensions = new MMCvImageDimensions();
  imageDimensions.getDimensions(tNode);
  if ((imageDimensions.width  > MMCvCaptionImageReplaceWidth)
      || (imageDimensions.height > MMCvCaptionImageReplaceHeight))
  { replaceImgTblNode = 1; }
  MMCvViewerObject.removeChild(tHideDiv);
 }
 else if ("TABLE" == inNode.tagName)
 { replaceImgTblNode = 1; }
 if (replaceImgTblNode)
 {
  var newImgNode = document.createElement('img');
  newImgNode.setAttribute('src', MMCvCaptionReplaceImg);
  newImgNode.setAttribute('width', '10');
  newImgNode.setAttribute('height', '9');
  newImgNode.setAttribute('alt', 'other content');
  newImgNode.style.border = '0px';
  var newANode = document.createElement('A');
  newANode.setAttribute('href', currentSourceAnchorName);
  newANode.style.border = "0px";
  newANode.appendChild(newImgNode);
  var parent = inNode.parentNode;
  parent.replaceChild(newANode, inNode);
  return; 
 }
 for (j=0; j < inNode.childNodes.length; j++)
 { this.augmentMMCvElementHierarchy(inNode.childNodes[j]); }
}

MMCvComponent.prototype.getMyNode = function ()
{
 var newNode = null;
 if ((this.myType < 0) || (MMC_TYPE_COUNT <= this.myType))
 { return newNode; }
 var mmcLinkHref = "";
 var mmcImgSrc   = "";
 var mmcImgAlt   = "";
 var mmcImgTitle = "";
 var mmcLinkName = "";
 var mainSrcTableLinks = getElementsByClassName(this.sourceNode, 'TABLE', this.myID);
 var mainSrcLinks = getElementsByClassName(mainSrcTableLinks[0], 'A', 'MMCvLINK');
 if (0 == mainSrcLinks.length)
 { return newNode; }
 var mainSrcMMCs = getElementsByClassName(mainSrcTableLinks[0], 'IMG', 'MMCvIMAGE_SRC');
 var sourceAnchors = getElementsByClassName(this.sourceNode, 'A', 'MMCvANCHOR_SRC');
 mmcLinkHref = mainSrcLinks[0].getAttribute("href");
 mmcLinkHref += "?MMCv=widget";
 mmcLinkName = sourceAnchors[0].name;
 if (mainSrcMMCs.length){
 mmcImgSrc   = mainSrcMMCs[0].getAttribute("src");
 mmcImgAlt   = mainSrcMMCs[0].getAttribute("alt");
 mmcImgTitle = mainSrcMMCs[0].getAttribute("title");
 this.sourceDimensions.getDimensions(mainSrcMMCs[0]);
 var widthScaling = 1;
 var heightScaling = 1;
 var scalingAxis = 'none';
 if (this.sourceDimensions.width > MMCvVideoImageMaxWidth)
 {
  widthScaling = this.sourceDimensions.width / MMCvVideoImageMaxWidth;
  scalingAxis = 'width';
 }
 if (this.sourceDimensions.height > MMCvImageMaxHeight)
 {
  heightScaling = this.sourceDimensions.height / MMCvVideoImageMaxHeight;
  if (heightScaling > widthScaling)
  { scalingAxis = 'height'; }
 }
 }

/*  To check "Thumbnail" image's height, width and to resize it if greater than maximum height, width .
 
   The imageString will be used to display the "Thumbnail/icon" image for the Video content ( without FLV) and to display the icon image in the widget if FLV content exists but not playable. 
   
   Since the "Icon" image will be in (50*50) which is lesser than the "Thumbnail" images maximum height and width . The icon image will not be resized and it will be displayed as it is.

 */

  if (this.imageHeight != '0' && this.imageWidth != '0') {
   videoImageText = "Click image to view video (opens in new window)";
  } else {
   videoImageText = "Click icon to view video (opens in new window)";
  }

  var videoErrorString = '<div style="width: '+ videoNoFlashWidth +'px; height: '+ videoNoFlashHeight +'px;"><div class="MMCvError">';
  videoErrorString += "To view this video, you need to download the <a href='http://get.adobe.com/flashplayer'>latest Adobe&reg; Flash Player</a>.<br><br>";
  videoErrorString += "Note: You can download the original video using the \"Download this Video\" link below or view it in a new window by clicking the video icon below. <br><br>";
  var imageString = "<a onclick=\"openNS('" + mmcLinkHref + "',700,500); return false;\" style=\"cursor:pointer\"><img src='" + mmcImgSrc + "'";
  //var imageString = "<a href=\""+mmcLinkHref+"\" style=\"cursor:pointer\"><img src='" + mmcImgSrc + "'";
 if ("none" != scalingAxis)
 {
  if ("width" == scalingAxis)
  { imageString += " width='" + MMCvVideoImageMaxWidth + "'"; }
  if ("height" == scalingAxis)
  { imageString += " height='" + MMCvVideoImageMaxHeight + "'"; }
 }
 if ("" != mmcImgAlt)
 { imageString += " alt='" + mmcImgAlt + "'"; }
 if ("" != mmcImgTitle)
 { imageString += " title='" + mmcImgTitle + "'"; }
 imageString += " style='border:0px;";
 if ("none" != scalingAxis)
 {
  if ("width" == scalingAxis)
  { imageString += " width: " + MMCvVideoImageMaxWidth + ";" + " height: " + this.sourceDimensions.height/widthScaling +";"; }
  if ("height" == scalingAxis)
  { imageString += " height: " + MMCvVideoImageMaxHeight + ";" + " width: " + this.sourceDimensions.height/heightScaling +";"; } 
 }
 imageString += "'/></a>";
 videoErrorString += imageString;
 videoErrorString += '</div></div>';

 // The message that will be displayed for Audio content, when FlashPlayer is not installed.

 var errorString = 'To listen to this audio, you need to download the <a href="http://get.adobe.com/flashplayer">latest Adobe&reg; Flash Player</a>.<br><br>';
 errorString += "Note: You can download and play the original audio using the \"Download this Audio\" link or by clicking the audio icon below.";

 var noCaptionErrorString = 'To listen to this audio, you need to download the <a href="http://get.adobe.com/flashplayer">latest Adobe&reg; Flash Player</a>.<br><br>';
  noCaptionErrorString += "Note: You can download and play the audio using the \"Download this Audio\" link or by clicking the audio icon below.";


 var audioErrorString = "<div class='MMCvAudioError'>"+errorString+"</div>";

 var componentClassName = "";
 componentClassName = 'MMCv';
 if (MMCvVIDEO == this.myType)
 { componentClassName += 'Video'; }
 else if (MMCvAUDIO == this.myType)
 { componentClassName += 'Audio'; }
 else 
 { componentClassName += 'Other'; }
 componentClassName += 'Component';
 newNode = document.createElement('div');
 if (null == newNode)
 { return newNode; }
 newNode.className = componentClassName;

 var downloadPhrase;
 if(MMCvVIDEO == this.myType)
  {downloadPhrase = "Video";}
 else if (MMCvAUDIO == this.myType)
  {downloadPhrase = "Audio";}
 else
  {downloadPhrase = "File";}

 var captionExists = getElementsByClassName(this.sourceNode, 'SPAN', 'MMCvCAPTION_SRC');

 var htmlString = ""; 
 var flashId = this.myID + "WIDGET";
 if (MMCvVIDEO == this.myType){
  if (this.MMCvFlashVars){
   htmlString += '<div class="MMCvVideoPlayerArea"><div class="MMCvVideoPlayerWrapper"><div id ="'+ flashId +'"  class="MMCvVideoPlayer">'+videoErrorString+'</div></div></div>';
  }else{
   htmlString += '<div class="MMCvVideoImageArea"><div class="MMCvVideoImageWrapper"><div class="MMCvVideoImage">';
   htmlString += imageString + '<br/>';
   htmlString += videoImageText;
   htmlString += '</div></div></div>';
  }
  htmlString += '<div class="MMCvVideoCaptionArea"><div class="MMCvVideoCaptionWrapper"><div class="MMCvVideoCaption"></div></div><div class="MMCvMoreWrapper"><a onclick="showMoreMMCvCaption();" style="cursor:pointer">More...</a></div><div class="MMCvLessWrapper"><a onclick="showLessMMCvCaption();" style="cursor:pointer">Less</a></div></div>';
  }else{
   if (!captionExists.length && MMCvAUDIO == this.myType && flashPlayerVersion != "supported" && !isAAI2Disabled) {
    htmlString += '<div class="MMCvAudioErrorNoCaption">'+noCaptionErrorString;
    htmlString += "<br/><br/><a href=\""+mmcLinkHref+"\" onclick=\"openNS('" + mmcLinkHref + "',700,500); return false;\" style=\"cursor:pointer\"><img src='"+ this.articleImageURL +"' border ='0'/></a><br/><br/></div>";
   }else{
    htmlString += '<div class="MMCvAudioOtherWrapper">';
   if ( MMCvAUDIO == this.myType && flashPlayerVersion != "supported" && !isAAI2Disabled) {
    htmlString += audioErrorString; 
   }
   htmlString += '<div class="MMCvCaptionArea"><div class="MMCvCaptionWrapper"><div class="MMCvCaption"></div></div><div class="MMCvMoreWrapper"><a onclick="showMoreMMCvCaption();" style="cursor:pointer">More...</a></div><div class="MMCvLessWrapper"><a onclick="showLessMMCvCaption();" style="cursor:pointer">Less</a></div></div><div class="MMCvImageArea"><div class="MMCvImage">';
  if ( MMCvAUDIO == this.myType && flashPlayerVersion != "supported" && !isAAI2Disabled) {
   htmlString += "<a href=\""+mmcLinkHref+"\" onclick=\"openNS('" + mmcLinkHref + "',700,500); return false;\" style=\"cursor:pointer\"><img src='"+ this.articleImageURL +"' border ='0'";
  }else {
   htmlString += "<a href=\""+mmcLinkHref+"\" onclick=\"openNS('" + mmcLinkHref + "',700,500); return false;\" style=\"cursor:pointer\"><img src='"+ this.thumbURL +"' border ='0'";
  }
  if (this.imageHeight != '0' && this.imageWidth != '0'){
   htmlString += "width=\""+ this.imageWidth+ "\" height =\""+ this.imageHeight+"\"/></a></div></div>";
  }else{
   htmlString += "/></a></div></div>";
  }
  if (MMCvAUDIO == this.myType && !isAAI2Disabled && flashPlayerVersion == "supported"){
   htmlString += '<div class="MMCvAudioPlayerWrapper"><div id ="'+flashId+'"  class="MMCvAudioPlayer"></div></div></div>';
  }else{
   htmlString += '</div>';
  }
 }
}
 htmlString +='<div class="MMCvViewDownloadWrapper"><div class="MMCvDownload">' + "<a href = '" + mmcLinkHref + "' onclick=\"openNS('" + mmcLinkHref + "',700,500); return false;\" style=\"cursor:pointer\">Download this "+downloadPhrase+"<\a>" + '</div><div class="MMCvViewWithin">' + "<a href='#" + mmcLinkName + "'>View within " + MMCvDocLabel + '</a></div></div>';
 newNode.innerHTML = htmlString;
 if (MMCvVIDEO == this.myType){
  caption = 'MMCvVideoCaption';
 }else{
  caption = 'MMCvCaption';
 }
 var newCaptionNodes = getElementsByClassName(newNode, 'DIV', caption);
 var newCaptionNode = newCaptionNodes[0];
 // To support DTD data
 var parentNode;
 var parentNodeClass;
 var mainLabels = getElementsByClassName(this.sourceNode, 'SPAN', 'MMCvLABEL_SRC');
 if (mainLabels.length) {
  for (i=0; i < mainLabels.length; i++) 
   {
    parentNode = mainLabels[i].parentNode;
    parentNodeClass = parentNode.getAttribute('class');
    if(!parentNodeClass) { parentNodeClass = parentNode.getAttribute('className'); }
     if (parentNodeClass && parentNodeClass != 'MMCvCAPTION_SRC') {
      var clonedLabelNode =  mainLabels[i].cloneNode(1);
      newCaptionNode.appendChild(clonedLabelNode);
      var spaceNode = document.createTextNode('.\u00A0');
      if (null != spaceNode) {
       newCaptionNode.appendChild(spaceNode);
      }
     }
   }
 }
 var mainCaptions = getElementsByClassName(this.sourceNode, 'SPAN', 'MMCvCAPTION_SRC');
 var mmcLabels;
 var mmcLabelLinks;
 var clonedCaptionNode;
 var newLabelLinkNode;
 var breakElement;
 var i;
 for (i=0; i < mainCaptions.length; i++)
 {
  clonedCaptionNode = mainCaptions[i].cloneNode(1);
  mmcLabels = getElementsByClassName(clonedCaptionNode, 'SPAN', 'MMCvLABEL_SRC');
  if (mmcLabels.length)
  {
   mmcLabelLinks = clonedCaptionNode.getElementsByTagName('A');
   if (0 == mmcLabelLinks.length)
   {
    newLabelLinkNode = document.createElement('a');
    if (null == newLabelLinkNode)
    { return null; }
    newLabelLinkNode.setAttribute('href', mmcLinkHref);
    while (null != mmcLabels[0].firstChild)
    {
     newLabelLinkNode.appendChild( mmcLabels[0].removeChild(mmcLabels[0].firstChild));
    }
    mmcLabels[0].appendChild(newLabelLinkNode);
   } else {
    mmcLabelLinks[0].href = mmcLinkHref;
    mmcLabelLinks[0].onclick = function(){openNS(mmcLinkHref,700,500);return false;};
   }
  }
  currentSourceAnchorName = "#" + mmcLinkName; 
  this.augmentMMCvElementHierarchy(clonedCaptionNode);
  currentSourceAnchorName = "";
  this.repositionInnerRefHovs(clonedCaptionNode);
  newCaptionNode.appendChild(clonedCaptionNode);
 }
 return newNode;
}

MMCvComponent.prototype.resetCaptionHeight = function ()
{
 var captionWrapper;
 var caption;
 if (MMCvVIDEO == this.myType){
  captionWrapper = 'MMCvVideoCaptionWrapper';
  caption = 'MMCvVideoCaption';
 }else{
  captionWrapper = 'MMCvCaptionWrapper';
  caption = 'MMCvCaption';
 }
 var currentMMC = this.MMCvNode;
 var currentCaptionWrapperElements = getElementsByClassName( currentMMC, 'DIV', captionWrapper);
 if (currentCaptionWrapperElements.length)
 {
  var currentCaptionElements = getElementsByClassName( currentCaptionWrapperElements[0], 'DIV', caption);
  if (currentCaptionElements.length)
  {
   var currentLessCaptionElements = getElementsByClassName( currentMMC, 'DIV', 'MMCvLessWrapper');
   if ('block' == currentLessCaptionElements[0].style.display)
   {
    if (currentCaptionElements[0].offsetHeight > MMCvHeightStandardCaption)
    {
     currentCaptionWrapperElements[0].style.height = currentCaptionElements[0].offsetHeight + MMCvHeightLessPhrase;
    }
    else
    {
     currentCaptionWrapperElements[0].style.height = MMCvHeightStandardCaption + "px";
     currentLessCaptionElements[0].style.display = 'none';
    }
   }
   else
   {
    var currentMoreCaptionElements = getElementsByClassName( currentMMC, 'DIV', 'MMCvMoreWrapper');
    if (currentCaptionElements[0].offsetHeight > MMCvHeightStandardCaption)
    {
     currentCaptionWrapperElements[0].style.height = MMCvHeightLessCaption + "px";
     currentMoreCaptionElements[0].style.display = 'block';
    }
    else
    {
     currentCaptionWrapperElements[0].style.height = MMCvHeightStandardCaption + "px";
     currentMoreCaptionElements[0].style.display = 'none';
    }
   }
  }
 }
 return 1;
}

function MMCvGetTitle()
{
 var prodImgPath = document.getElementById('MMCvProdImgPath').getAttribute('src');
 var htmlString = '<div id="multiMediaTitle" class="MMCvTitleBox"><span class="MMCvTitleSpan" style="border-bottom:2px solid #FFFFFF; top:2;"><img src="' + prodImgPath + '" alt="" style="vertical-align: middle;" /> </span><span id="multiMediaContentTitle" class ="MMCvTitleSpan" style="vertical-align:middle;">Supplementary Content within this ' + MMCvDocLabel + '</span></div>';
 return htmlString;
}

function MMCvGetTabs()
{
 var EXISTING_TAB_COUNT = 0;
 var htmlString = "";
 var activeTab = MMC_TYPE_COUNT; // NOT_SET
 if (MMCvComponents[MMCvVIDEO].length)
 {
  if (MMC_TYPE_COUNT == activeTab)
  { activeTab = MMCvVIDEO; }
  EXISTING_TAB_COUNT++;
 }
 if (MMCvComponents[MMCvAUDIO].length)
 {
  if (MMC_TYPE_COUNT == activeTab)
  { activeTab = MMCvAUDIO; }
  EXISTING_TAB_COUNT++;
 }
 if (MMCvComponents[MMCvOTHER].length)
 {
  if (MMC_TYPE_COUNT == activeTab)
  { activeTab = MMCvOTHER; }
  EXISTING_TAB_COUNT++;
 }

 htmlString  = '<div id="multiMediaTabs" class="MMCvTabs">';
 if (EXISTING_TAB_COUNT >= 2)
 {
  if (MMCvComponents[MMCvVIDEO].length)
  {
   htmlString += '<span id="MMCvVideoTab" class="MMCvTab';
   if (MMCvVIDEO == activeTab)
   { htmlString += 'Active'; }
   htmlString += '"><a onclick="goToMMCvTab(MMCvVIDEO);">Video ('+MMCvComponents[MMCvVIDEO].length+')</a> </span>';
  }
  if (MMCvComponents[MMCvAUDIO].length)
  {
   htmlString += '<span id="MMCvAudioTab" class="MMCvTab';
   if (MMCvAUDIO == activeTab)
   { htmlString += 'Active'; }
   htmlString += '"><a onclick="goToMMCvTab(MMCvAUDIO);">Audio ('+MMCvComponents[MMCvAUDIO].length+')</a> </span>';
  }
  if (MMCvComponents[MMCvOTHER].length)
  {
   htmlString += '<span id="MMCvOtherTab" class="MMCvTab';
   if (MMCvOTHER == activeTab)
   { htmlString += 'Active'; }
   htmlString += '"><a onclick="goToMMCvTab(MMCvOTHER);">Other Files ('+MMCvComponents[MMCvOTHER].length+')</a> </span>';
  }
 }
 htmlString += '</div>';
 return htmlString;
}

function MMCvGetSelector()
{
 var htmlString = '<div id="multiMediaSelector" class="MMCvSelector"></div>';
 return htmlString;
}

function MMCvGetViewerStart()
{
 var htmlString ='<div id="multiMediaViewBacking" class="MMCvViewBacking"><div id="multiMediaViewer" class="MMCvViewer">';
 return htmlString;
}

function MMCvGetViewerEnd()
{
 var htmlString = '</div></div>';
 return htmlString;
}

function MMCvLookForFontChangeEvents()
{
 MMCvHeight1Line = parseInt(MMCvSizer1.offsetHeight);
 if (MMCvHeight1Line != MMCvHeight1Prev) 
 {
  MMCvHeight1Prev = MMCvHeight1Line;
  MMCvCalculateHeights();
  MMCvComponents[currentMMCtype][currentMMCindex].resetCaptionHeight();
  resetContainerHeights(MMCvComponents[currentMMCtype][currentMMCindex].MMCvNode);
  resizeSelectorArrows();
 }
 timeoutID2 = setTimeout("MMCvLookForFontChangeEvents()", 1000);
}

function MMCvCalculateHeights()
{
 var i;
 var j;
 var currentMMC;
 var moreHeightObjectFound = 0;
 var lessHeightObjectFound = 0;
 var viewWithinHeightObjectFound = 0;
 var moreCaptionElements;
 var lessCaptionElements;
 var viewWithinElements;
 var saveSetting;
 var MMCvMorePhraseObject;
 var MMCvLessPhraseObject;
 var MMCvViewWithinPhraseObject;
 var MMCvHeightViewWithinPhrase = 0;
 for (i=0; i < MMC_TYPE_COUNT; i++)
 {
  for (j=0; j < MMCvComponents[i].length; j++)
  {
   MMCvComponents[i][j].initialize();
   if (null != MMCvComponents[i][j].MMCvNode)
   {
    currentMMC = MMCvComponents[i][j].MMCvNode;
    if (!moreHeightObjectFound)
    {
     moreCaptionElements = getElementsByClassName(currentMMC,'DIV','MMCvMoreWrapper');
     if (moreCaptionElements.length)
     {
      MMCvMorePhraseObject = moreCaptionElements[0];
      saveSetting = 'none';
      if (MMCvMorePhraseObject.style.display)
      { saveSetting = MMCvMorePhraseObject.style.display; }
      MMCvMorePhraseObject.style.display = 'block';
      MMCvHeightMorePhrase = MMCvMorePhraseObject.offsetHeight;
      MMCvMorePhraseObject.style.display = saveSetting;
      moreHeightObjectFound = 1;
     }
    }
    if (!lessHeightObjectFound)
    {
     lessCaptionElements = getElementsByClassName(currentMMC,'DIV','MMCvLessWrapper');
     if (lessCaptionElements.length)
     {
      MMCvLessPhraseObject = lessCaptionElements[0];
      saveSetting = 'none';
      if (MMCvLessPhraseObject.style.display)
      { saveSetting = MMCvLessPhraseObject.style.display; }
      MMCvLessPhraseObject.style.display = 'block';
      MMCvHeightLessPhrase = MMCvLessPhraseObject.offsetHeight;
      MMCvLessPhraseObject.style.display = saveSetting;
      lessHeightObjectFound = 1;
     }
    }
    if (!viewWithinHeightObjectFound)
    {
     viewWithinElements = getElementsByClassName(currentMMC,'DIV','MMCvViewWithin');
     if (viewWithinElements.length)
     {
      MMCvViewWithinPhraseObject = viewWithinElements[0];
      MMCvHeightViewWithinPhrase = MMCvViewWithinPhraseObject.offsetHeight;
      viewWithinHeightObjectFound = 1;
     }
    }
    if (moreHeightObjectFound && lessHeightObjectFound )
    {
     i = MMC_TYPE_COUNT-1;
     j = MMCvComponents[i].length-1;
    }
   }
  }
 }
 MMCvHeightStandardCaption = MMCvHeightImageMax;
 return 1;
}

function embedPlayerArticle()
{
 var i;
 for (i=0; i< MMCvComponents[MMCvVIDEO].length; i++)
 {
  if (null != MMCvComponents[MMCvVIDEO][i].MMCvFlashVars)
  {
   var faceId = MMCvComponents[MMCvVIDEO][i].myID + "FLASH";
   var id = MMCvComponents[MMCvVIDEO][i].myID;
   myNode = document.getElementById(id).parentNode;
   var installFlashTag = getElementsByClassName(myNode, "SPAN", "MMCvInstallFP");
   if (installFlashTag.length){
    installFlashTag[0].innerHTML = "To view the video inline, you need to <a href='http://get.adobe.com/flashplayer'>download the latest Adobe&reg; Flash Player</a>. However, you can download and view the video by clicking on the icon below."
   }
   var flashvars = false;
   var params = { menu: "false",
                  play: "false",
		  bgcolor: "0xFFF",
                  allowscriptaccess:"always",
                  allowFullScreen:"true",
                  wmode: "opaque",
                  flashvars: MMCvComponents[MMCvVIDEO][i].MMCvFlashVars
                };

   swfobject.embedSWF(videoPlayerUrl
                    , faceId
                    , videoPlayerWidth 
                    , videoPlayerHeight 
                    , reqFlashVersion 
                    , false
                    , flashvars
                    , params);
   }
 }
 for (i=0; i< MMCvComponents[MMCvAUDIO].length; i++)
 {
  if (null != MMCvComponents[MMCvAUDIO][i].MMCvFlashVars)
  {
   var faceId = MMCvComponents[MMCvAUDIO][i].myID + "FLASH";
   var id = MMCvComponents[MMCvAUDIO][i].myID;
   myNode = document.getElementById(id).parentNode;
   var installFlashTag = getElementsByClassName(myNode, "SPAN", "MMCvInstallFP");
   if (installFlashTag.length){
    installFlashTag[0].innerHTML = "To listen to this audio, you need to <a href='http://get.adobe.com/flashplayer'>download the latest Adobe&reg;  Flash Player</a>. However, you can download and play the audio by clicking on the icon below."
   }
   var flashvars = false;
   var params = { menu: "false",
                  play: "false",
                  bgcolor: "0xFFF",
                  allowscriptaccess:"always",
                  wmode: "opaque",
                  flashvars: MMCvComponents[MMCvAUDIO][i].MMCvFlashVars
                };
   swfobject.embedSWF(audioPlayerUrl
                       , faceId
                       , audioPlayerWidth 
                       , audioPlayerHeight 
                       , reqFlashVersion 
                       , false
                       , flashvars
                       , params);
    } 
  }
}

MMCvComponent.prototype.embedPlayerWidget = function()
{
  this.MMCvFlashVars += "&MMCv=widget";
  var i = 0;
  var player = videoPlayerUrl;
  var height = videoPlayerHeight;
  var width = videoPlayerWidth;
  if (MMCvAUDIO == this.myType){
   player = audioPlayerUrl;
   height = audioPlayerHeight;
   width = audioPlayerWidth;
  }
  var faceId = this.myID + "WIDGET";
  var flashvars = false;
  var params = { menu: "false",
                 play: "false",
                 bgcolor: "0xFFF",
                 allowscriptaccess:"always",
                 allowFullScreen:"true",
	         wmode: "opaque",
	         flashvars: this.MMCvFlashVars
               };
     swfobject.embedSWF(player
                        , faceId
			, width 
			, height
                        , reqFlashVersion 
                        , false
			, flashvars
                        , params );
}
function MMCinit()
{
 if (!MMCinitialized)
 {
  if(!isAAI2Disabled)
  {
   embedPlayerArticle();
  }
  var artBody = document.getElementById('articleBody');
  if (artBody != null)
  {var refHovsObject = document.createElement('div');
   refHovsObject.id = "multiMediaRefHovs";
   refHovsObject.className = "MMCvRefHovs";
   artBody.insertBefore(refHovsObject, artBody.firstChild);
  }
  MMCvRow = document.getElementById('multiMediaViewerRow');
  if (null == MMCvRow)
  { return; }
  MMCvOuterContainer = document.getElementById('multiMediaViewerOuter');
  if (null == MMCvOuterContainer)
  { return; }
  if (!MMCvContentExists())
  { return; }
  var MMCvContent = MMCvGetTitle();
  MMCvContent += MMCvGetTabs();
  MMCvContent += MMCvGetSelector();
  MMCvContent += MMCvGetViewerStart();
  MMCvContent += MMCvSizerHTML;
  MMCvContent += MMCvGetViewerEnd();
  MMCvOuterContainer.innerHTML = MMCvContent;
  MMCvRow.style.display="block";
  MMCvTitleObject = document.getElementById('multiMediaTitle');
  MMCvRefHovsObject = document.getElementById('multiMediaRefHovs');
  MMCvTabsObject = document.getElementById('multiMediaTabs');
  MMCvSelectorObject = document.getElementById('multiMediaSelector');
  if (null == MMCvSelectorObject)
  { MMCvRow.style.display="none"; return; }
  MMCvViewBackingObject = document.getElementById('multiMediaViewBacking');
  if (null == MMCvViewBackingObject)
  { MMCvRow.style.display="none"; return; }
  MMCvViewerObject = document.getElementById('multiMediaViewer');
  if (null == MMCvViewerObject)
  { MMCvRow.style.display="none"; return; }
  MMCvSizer1 = document.getElementById('MMCvSizer1');
  if (null == MMCvSizer1)
  { MMCvRow.style.display="none"; return; }
  MMCvHeight1Line = parseInt(MMCvSizer1.offsetHeight);
  MMCvHeight1Prev = MMCvHeight1Line;
  MMCvCalculateHeights();
  if (MMCvComponents[MMCvVIDEO].length) { goToMMCvTab(MMCvVIDEO); }
  else if (MMCvComponents[MMCvAUDIO].length) { goToMMCvTab(MMCvAUDIO); }
  else if (MMCvComponents[MMCvOTHER].length) { goToMMCvTab(MMCvOTHER); }
  MMCinitialized = 1;
  MMCvLookForFontChangeEvents();
 }
}
function getMMCvSelectorHTML(tabID) 
{
 var htmlString = "";
 var j;
 if ("" == MMCvSelectorHTML[tabID])
 {
  if (MMCvComponents[tabID].length)
  {
  htmlString = '<div class="MMCvSelectDiv"><span id="MMCvPrevInactive" class="MMCvSelRover">' + "<img src='/scidirimg/prevArrowInactive.gif' width='28px' height='19px' />" + '</span><span id="MMCvPrevActive" class="MMCvSelRover" style="display: none"><span class="MMCvSelectorItem"><a onmouseover="prevArrowOnMouseOver();" onmouseout="prevArrowOnMouseOut();" onclick="shiftMMCvLeft(); return false;" href="#">' + "<img id='prevArrow' src='/scidirimg/prevArrowActive.gif' width='28px' height='19px' alt='Previous item' title='Previous item' />" + '</a></span></span>';
   for (j=0; j < MMCvComponents[tabID].length; j++)
   {
    if ((MMCvComponents[tabID].length-1) == j )
    {
     htmlString += '<a onclick="expandMMCvRight();" class="MMCvSelectorCollapseItem" style="display: none;"> &#8230</a>'; 
    }
    htmlString += '<span class="MMCvSelectorItem"> <a onclick="goToMMCvComponent(' + j + ');">';
    htmlString += j+1
    htmlString += '</a></span>';
    if (0 == j)
    {
     htmlString += '<a onclick="expandMMCvLeft();" class="MMCvSelectorCollapseItem" style="display: none;"> &#8230</a>';
    }
   }
   htmlString += ' <span id="MMCvNextActive" class="MMCvSelRover"';
   if (1 == MMCvComponents[tabID].length)
   { htmlString += ' style="display: none"'; }
   htmlString += '><span class="MMCvSelectorItem"><a onmouseover="nextArrowOnMouseOver();" onmouseout="nextArrowOnMouseOut();" onclick="shiftMMCvRight(); return false" href="#">' + "<img id='nextArrow' src='/scidirimg/nextArrowActive.gif' width='28px' height='19px' alt='Next item' title='Next item' />" + '</a></span></span><span id="MMCvNextInactive" class="MMCvSelRover"';
   if (1 < MMCvComponents[tabID].length)
   { htmlString += ' style="display: none;"'; }
   htmlString += '>'+"<img src='/scidirimg/nextArrowInactive.gif' width='28px' height='19px' />"+'</span></div>';
   MMCvSelectorHTML[tabID] = htmlString;
  }
 }
 return MMCvSelectorHTML[tabID];
}

function mapMMCvSelectorItems(tabID)
{
 MMCvSelectorItems[tabID] = getElementsByClassName(MMCvSelectorObject, 'SPAN', 'MMCvSelectorItem');
 MMCvSelectorCollapseItems[tabID] = getElementsByClassName(MMCvSelectorObject, 'A', 'MMCvSelectorCollapseItem');
}

function saveSelector()
{
 if ((currentMMCtype >= 0) && (currentMMCtype <= MMC_TYPE_COUNT))
 {
  var selectorObject = MMCvSelectorObject.removeChild(MMCvSelectorObject.firstChild);
  if (null == MMCvSelectorLists[currentMMCtype])
  { MMCvSelectorLists[currentMMCtype] = new MMCvSelectorList(currentMMCtype,selectorObject,currentMMCindex); }
  else
  { MMCvSelectorLists[currentMMCtype].currentIndex = currentMMCindex; }
 }
}

function resizeSelectorArrows()
{
 var vw;
 var parentStyle;
 var parentSize;
 var myStyle;
 var currentSize;
 var factor;
 var targetSize = 17.5;
 var rehide = 0;
 if (document.defaultView)
 {
  vw = document.defaultView;
  if (null != MMCvSelectorPrevActive)
  {
   if ('none' == MMCvSelectorPrevActive.style.display)
   {
    MMCvSelectorPrevActive.style.display = 'inline';
    rehide = 1;
   }
   parentStyle = vw.getComputedStyle(MMCvSelectorPrevActive.parentNode,"");
   parentSize = parseFloat(parentStyle.getPropertyValue("font-size"));
   factor = (targetSize / parentSize) * 100;
   MMCvSelectorPrevActive.style.fontSize = factor + "%";
   if (rehide)
   { MMCvSelectorPrevActive.style.display = 'none'; }
  }
  rehide = 0;
  if (null != MMCvSelectorNextActive)
  {
   if ('none' == MMCvSelectorNextActive.style.display)
   { MMCvSelectorNextActive.style.display = 'inline'; rehide = 1; }
   parentStyle = vw.getComputedStyle(MMCvSelectorNextActive.parentNode,"");
   parentSize = parseFloat(parentStyle.getPropertyValue("font-size"));
   factor = (targetSize / parentSize) * 100;
   MMCvSelectorNextActive.style.fontSize = factor + "%";
   if (rehide)
   { MMCvSelectorNextActive.style.display = 'none'; }
  }
 }
 return 1;
}

function resetSelectorArrows()
{
 MMCvSelectorPrevInactive = document.getElementById('MMCvPrevInactive');
 MMCvSelectorPrevActive = document.getElementById('MMCvPrevActive');
 MMCvSelectorNextInactive = document.getElementById('MMCvNextInactive');
 MMCvSelectorNextActive = document.getElementById('MMCvNextActive');
 resizeSelectorArrows();
}

function showSelector()
{
 if (null == MMCvSelectorLists[currentMMCtype])
 {
  MMCvSelectorObject.innerHTML = getMMCvSelectorHTML(currentMMCtype);
  mapMMCvSelectorItems(currentMMCtype);
  resetSelectorArrows();
  updateMMCvSelectorIndex(-1, 0);
 }
 else
 {
  MMCvSelectorObject.appendChild(MMCvSelectorLists[currentMMCtype].MMCvNode);
  resetSelectorArrows();
  updateMMCvSelectorIndex(MMCvSelectorLists[currentMMCtype].currentIndex,0,1);
 }
}

function indexInMMCvSelectorRange(inIndex)
{
 var inRange = 1;
 if (gapLSelectorStart >= 0)
 {
  if ((gapLSelectorStart <= (inIndex +1)) && ((inIndex +1) <= gapLSelectorEnd) )
  { inRange = 0; }
 }
 if ((inRange) && (gapRSelectorStart >= 0))
 {
  if ((gapRSelectorStart <= (inIndex +1)) && ((inIndex +1) <= gapRSelectorEnd) )
  { inRange = 0; }
 }
 return inRange;
}

function updateMMCvSelectorRange(oldIndex, newIndex)
{
 var componentCount = MMCvComponents[currentMMCtype].length;
 if (componentCount <= (MMCvSelectorMidWidth + 4))
 {
  gapLSelectorStart = -1;
  gapLSelectorEnd   = -1;
  gapRSelectorStart = -1;
  gapRSelectorEnd   = -1;
  return;
 }
 var local_gapLSelectorStart = -1;
 var local_gapLSelectorEnd   = -1;
 var local_gapRSelectorStart = -1;
 var local_gapRSelectorEnd   = -1;
 var startDisplayRange       = 3;
 var endDisplayRange         = MMCvSelectorMidWidth+2;
 if ((oldIndex >= 0) && ((newIndex - oldIndex) < MMCvSelectorMidWidth))
 {
  if (oldIndex < newIndex)
  { startDisplayRange = oldIndex + 1; endDisplayRange = newIndex + 1; }
  else
  { startDisplayRange = newIndex + 1; endDisplayRange = oldIndex + 1; }
  var openWidth = MMCvSelectorMidWidth - (endDisplayRange - startDisplayRange + 1);
  if ((startDisplayRange - openWidth) <= 3)
  {
   local_gapLSelectorStart = -1;
   local_gapLSelectorEnd = -1;
   startDisplayRange = 2;
   endDisplayRange = MMCvSelectorMidWidth+2;
   local_gapRSelectorStart = MMCvSelectorMidWidth+3;
   local_gapRSelectorEnd   = componentCount - 1;
  }
  else if ((endDisplayRange + openWidth) >= (componentCount - 2))
  {
   local_gapLSelectorStart = 2;
   local_gapLSelectorEnd = componentCount - (MMCvSelectorMidWidth + 2);
   startDisplayRange = local_gapLSelectorEnd + 1;
   endDisplayRange = componentCount - 1;
   local_gapRSelectorStart = -1;
   local_gapRSelectorEnd   = -1;
  }
  else
  {
   startDisplayRange -= (openWidth / 2);
   endDisplayRange = startDisplayRange + MMCvSelectorMidWidth - 1;
   local_gapLSelectorStart = 2;
   local_gapLSelectorEnd = startDisplayRange - 1;
   local_gapRSelectorStart = endDisplayRange + 1;
   local_gapRSelectorEnd = componentCount - 1;
  }
 }
 else
 {
  if (newIndex < (MMCvSelectorMidWidth+2))
  {
   startDisplayRange = 2;
   endDisplayRange = MMCvSelectorMidWidth+2;
   local_gapLSelectorStart = -1;
   local_gapLSelectorEnd = -1;
   local_gapRSelectorStart = MMCvSelectorMidWidth+3;
   local_gapRSelectorEnd = componentCount - 1;
  }
  else if (newIndex >= (componentCount - (MMCvSelectorMidWidth + 2)))
  {
   local_gapLSelectorStart = 2;
   local_gapLSelectorEnd = componentCount - (MMCvSelectorMidWidth + 2);
   startDisplayRange = local_gapLSelectorEnd + 1;
   endDisplayRange = componentCount - 1;
   local_gapRSelectorStart = -1;
   local_gapRSelectorEnd = -1;
  }
  else 
  {
   local_gapLSelectorStart = 2;
   local_gapLSelectorEnd = newIndex - (MMCvSelectorMidWidth / 2) - 1;
   startDisplayRange = local_gapLSelectorEnd + 1;
   endDisplayRange = startDisplayRange + MMCvSelectorMidWidth - 1;
   local_gapRSelectorStart = endDisplayRange + 1;
   local_gapRSelectorEnd = componentCount - 1;
  }
 }
 resetSelectorDisplay(local_gapLSelectorStart, local_gapLSelectorEnd, startDisplayRange, endDisplayRange, local_gapRSelectorStart, local_gapRSelectorEnd);
}

function resetSelectorDisplay(in_gapLSelectorStart, in_gapLSelectorEnd, in_startDisplayRange, in_endDisplayRange, in_gapRSelectorStart, in_gapRSelectorEnd)
{
 var i;
 if (in_gapLSelectorStart >= 0)
 {  
  MMCvSelectorCollapseItems[currentMMCtype][0].style.display="inline";
  for (i=2; i <= in_gapLSelectorEnd; i++)
  { MMCvSelectorItems[currentMMCtype][i].style.display="none"; }
 }
 else
 {
  MMCvSelectorCollapseItems[currentMMCtype][0].style.display="none";
 }
 //___ Show links in range.
 for (i=in_startDisplayRange; i <= in_endDisplayRange; i++)
 {
  MMCvSelectorItems[currentMMCtype][i].style.display="inline";
 }
 if (in_gapRSelectorStart >= 0)
 {  
  for (i=in_gapRSelectorStart; i <= (in_gapRSelectorEnd); i++)
  { MMCvSelectorItems[currentMMCtype][i].style.display="none"; }
  MMCvSelectorCollapseItems[currentMMCtype][1].style.display="inline";
 }
 else
 { MMCvSelectorCollapseItems[currentMMCtype][1].style.display="none"; }
 gapLSelectorStart = in_gapLSelectorStart;
 gapLSelectorEnd   = in_gapLSelectorEnd;
 gapRSelectorStart = in_gapRSelectorStart;
 gapRSelectorEnd   = in_gapRSelectorEnd;
}

function expandMMCvLeft()
{
 var componentCount = MMCvComponents[currentMMCtype].length;
 var local_gapLSelectorStart = -1;
 var local_gapLSelectorEnd   = -1;
 var local_gapRSelectorStart = -1;
 var local_gapRSelectorEnd   = -1;
 var endDisplayRange = gapLSelectorEnd;
 var startDisplayRange = endDisplayRange - MMCvSelectorMidWidth + 1;

 if (startDisplayRange <= 3)
 { endDisplayRange += 3 - startDisplayRange; startDisplayRange = 1; }
 else
 { local_gapLSelectorStart = 2; local_gapLSelectorEnd   = startDisplayRange - 1; }
 if (endDisplayRange < (componentCount-2))
 { local_gapRSelectorStart = endDisplayRange + 1; local_gapRSelectorEnd   = componentCount-1; }
 else
 { endDisplayRange = componentCount-1; }
 resetSelectorDisplay(local_gapLSelectorStart, local_gapLSelectorEnd, startDisplayRange, endDisplayRange, local_gapRSelectorStart, local_gapRSelectorEnd);
 goToMMCvComponent(startDisplayRange-1);
}

function expandMMCvRight()
{
 var componentCount = MMCvComponents[currentMMCtype].length;
 var local_gapLSelectorStart = -1;
 var local_gapLSelectorEnd   = -1;
 var local_gapRSelectorStart = -1;
 var local_gapRSelectorEnd   = -1;
 var startDisplayRange = gapRSelectorStart; 
 var endDisplayRange = startDisplayRange + MMCvSelectorMidWidth - 1;
 if (endDisplayRange >= (componentCount - 2))
 { startDisplayRange -= (endDisplayRange - (componentCount-2)); endDisplayRange = componentCount - 1; }
 else
 { local_gapRSelectorStart = endDisplayRange + 1; local_gapRSelectorEnd   = componentCount - 1; }
 if (startDisplayRange > 3)
 { local_gapLSelectorStart = 2; local_gapLSelectorEnd   = startDisplayRange-1; }
 else
 { startDisplayRange = 2; }
 resetSelectorDisplay(local_gapLSelectorStart, local_gapLSelectorEnd, startDisplayRange, endDisplayRange, local_gapRSelectorStart, local_gapRSelectorEnd);
 goToMMCvComponent(startDisplayRange-1);
}

function updateMMCvSelectorIndex(oldIndex, newIndex, resetFlag)
{
 if ("undefined" == resetFlag)
 { resetFlag = 0; }

 if ((null != MMCvSelectorPrevInactive) && (null != MMCvSelectorPrevActive))
 {
  if ((0 == oldIndex) && (0 < newIndex))
  {
   MMCvSelectorPrevInactive.style.display = "none";
   MMCvSelectorPrevActive.style.display = "inline"
  }
  else if ((0 < oldIndex) && (0 == newIndex))
  {
   MMCvSelectorPrevInactive.style.display = "inline";
   MMCvSelectorPrevActive.style.display = "none"
  }
 }

 if ((null != MMCvSelectorNextInactive) && (null != MMCvSelectorNextActive))
 {
  if (((MMCvComponents[currentMMCtype].length-1) == oldIndex) && ((MMCvComponents[currentMMCtype].length-1) > newIndex))
  {
   MMCvSelectorNextInactive.style.display = "none";
   MMCvSelectorNextActive.style.display = "inline"
  }
  else if (((MMCvComponents[currentMMCtype].length-1) > oldIndex) && ((MMCvComponents[currentMMCtype].length-1) == newIndex))
  {
   MMCvSelectorNextInactive.style.display = "inline";
   MMCvSelectorNextActive.style.display = "none"
  }
 }
 if (oldIndex >= 0)
 { MMCvSelectorItems[currentMMCtype][oldIndex+1].style.fontWeight="normal"; }
 if (resetFlag)
 { oldIndex = -1; }
 if ((oldIndex < 0) || (! indexInMMCvSelectorRange(newIndex)))
 { updateMMCvSelectorRange(oldIndex, newIndex); }
 MMCvSelectorItems[currentMMCtype][newIndex+1].style.fontWeight="bold";
}

function getTabObject(tabID) 
{
 var lookForName='';
 if (MMCvVIDEO == tabID)
 { lookForName = 'Video'; }
 else if (MMCvAUDIO == tabID)
 { lookForName = 'Audio'; }
 else if (MMCvOTHER == tabID)
 { lookForName = 'Other'; }
 if ('' == lookForName)
 { return null; }
 var lookForTabName = 'MMCv' + lookForName + 'Tab';
 return document.getElementById(lookForTabName);
}

function swapTabs(tabID) 
{
 if ((currentMMCtype != tabID) && (tabID >= 0) && (tabID <= MMC_TYPE_COUNT))
 {
  if (currentMMCtype >= 0)
  {
   MMCvComponents[currentMMCtype][currentMMCindex].stopPlayer();
   var prevTab = getTabObject(currentMMCtype);
   if (null != prevTab)
   { prevTab.className = 'MMCvTab'; }
  }
  saveSelector();
  if ((currentMMCtype >= 0) && (currentMMCindex >= 0))
  {
   MMCvComponents[currentMMCtype][currentMMCindex].hideSelf();
  }
  currentMMCtype  = tabID;
  currentMMCindex = 0;
  var nextTab = getTabObject(tabID);
  if (null != nextTab)
  { nextTab.className = 'MMCvTabActive'; }
  showSelector();
  MMCvComponents[currentMMCtype][currentMMCindex].showSelf();
 }
}

function goToMMCvTab(tabID) 
{
 if(MMCvVIDEO == tabID){
   MMCvHeightStandardCaption = MMCvVideoCaptionHeightMax;
 }else{
   MMCvHeightStandardCaption = MMCvHeightImageMax;
 }
 if (MMCvHeightStandardCaption < (MMCvHeightMorePhrase + MMCvHeight1Line))
 {  // Force at least one line of caption and more phrase to be visible.
  MMCvHeightStandardCaption = MMCvHeightMorePhrase + MMCvHeight1Line;
 }
 MMCvHeightLessCaption = MMCvHeightStandardCaption - MMCvHeightMorePhrase;
 MMCvHeightComponentWrapper = MMCvHeightStandardCaption;
 swapTabs(tabID);
 MMCvComponents[currentMMCtype][currentMMCindex].resetCaptionHeight();
 resetContainerHeights(MMCvComponents[currentMMCtype][currentMMCindex].MMCvNode);
}

function showMoreMMCvCaption() 
{
 var captionWrapper;
 var caption;
 if (MMCvVIDEO == currentMMCtype) {
   captionWrapper = 'MMCvVideoCaptionWrapper';
   caption = 'MMCvVideoCaption';
 }else{
   captionWrapper = 'MMCvCaptionWrapper';
   caption = 'MMCvCaption';
 }
 var currentMMC = MMCvComponents[currentMMCtype][currentMMCindex].MMCvNode;
 var moreCaptionHeight = 0;
 var currentCaptionElements = getElementsByClassName(currentMMC,'DIV',caption);
 if (currentCaptionElements.length)
 { moreCaptionHeight = currentCaptionElements[0].offsetHeight; }
 var currentCaptionWrapperElements = getElementsByClassName( currentMMC, 'DIV',captionWrapper);
 var currentMoreWrapperElements = getElementsByClassName( currentMMC, 'DIV', 'MMCvMoreWrapper');
 var currentLessWrapperElements = getElementsByClassName( currentMMC, 'DIV', 'MMCvLessWrapper');
 var playerElements = getElementsByClassName(currentMMC, 'DIV', 'MMCvVideoPlayerWrapper');
 var viewerHeight = moreCaptionHeight;
 if (playerElements.length){
   viewerHeight += playerElements[0].offsetHeight;
 }
 var audioElements = getElementsByClassName(currentMMC, 'DIV', 'MMCvAudioPlayerWrapper');
 if (audioElements.length){
   viewerHeight += audioElements[0].offsetHeight;
 }
 var viewWithinElements = getElementsByClassName(currentMMC,'DIV','MMCvViewDownloadWrapper');
 if (viewWithinElements.length){
   viewerHeight += viewWithinElements[0].offsetHeight;
 }
 var videoImage = getElementsByClassName(currentMMC, 'DIV', 'MMCvVideoImageArea');
 if (videoImage.length){
   viewerHeight += videoImage[0].offsetHeight;
 }
 if (currentLessWrapperElements.length)
 {
  currentLessWrapperElements[0].style.display = 'block';
  viewerHeight += currentLessWrapperElements[0].offsetHeight;
 }
 var audioErrorElements = getElementsByClassName(currentMMC, 'DIV', 'MMCvAudioError');
 if (audioErrorElements.length){
   viewerHeight += audioErrorElements[0].offsetHeight;
 }

 MMCvViewerObject.style.height   = viewerHeight + "px";
 var viewBackingHeight = viewerHeight + 3;
 MMCvViewBackingObject.style.height   = viewBackingHeight + "px";
 currentCaptionWrapperElements[0].style.height = moreCaptionHeight + "px";
 currentMoreWrapperElements[0].style.display = 'none';
}

function showLessMMCvCaption() 
{
 var captionWrapper;
 if (MMCvVIDEO == currentMMCtype) {  
   captionWrapper = 'MMCvVideoCaptionWrapper';
 }else{
   captionWrapper = 'MMCvCaptionWrapper';
 }
 var currentMMC = MMCvComponents[currentMMCtype][currentMMCindex].MMCvNode;
 var currentCaptionWrapperElements = getElementsByClassName( currentMMC, 'DIV',captionWrapper);
 var currentMoreWrapperElements = getElementsByClassName( currentMMC, 'DIV', 'MMCvMoreWrapper');
 var currentLessWrapperElements = getElementsByClassName( currentMMC, 'DIV', 'MMCvLessWrapper');
 var playerElements = getElementsByClassName( currentMMC, 'DIV', 'MMCvVideoPlayerWrapper');
 var viewHeight = 0;
 if (playerElements.length){
    viewHeight += playerElements[0].offsetHeight;
 }
 var audioElements = getElementsByClassName( currentMMC, 'DIV', 'MMCvAudioPlayerWrapper');
 if (audioElements.length){
    viewHeight += audioElements[0].offsetHeight;
 }
 var viewWithinElements = getElementsByClassName(currentMMC,'DIV','MMCvViewDownloadWrapper');
 if (viewWithinElements.length){
    viewHeight += viewWithinElements[0].offsetHeight;
 }
 var videoImage =  getElementsByClassName( currentMMC, 'DIV', 'MMCvVideoImageArea');
 if (videoImage.length){
    viewHeight += videoImage[0].offsetHeight;
 }
 var audioErrorElements = getElementsByClassName(currentMMC, 'DIV', 'MMCvAudioError');
 if (audioErrorElements.length){
   viewHeight += audioErrorElements[0].offsetHeight;
 }

 viewHeight += MMCvHeightStandardCaption;
 MMCvViewerObject.style.height = viewHeight + "px";
 var viewBackingHeight = viewHeight + 3;
 MMCvViewBackingObject.style.height = viewBackingHeight + "px";
 currentCaptionWrapperElements[0].style.height = MMCvHeightLessCaption + "px";
 currentMoreWrapperElements[0].style.display = 'block';
 currentLessWrapperElements[0].style.display = 'none';
}

function resetContainerHeights(inComponent) 
{
 var targetViewerHeight = parseInt(inComponent.offsetHeight);
 var viewBackingHeight = targetViewerHeight + 1;
 MMCvViewerObject.style.height = targetViewerHeight + "px";
 MMCvViewBackingObject.style.height = viewBackingHeight + "px";
}

function goToMMCvComponent(newIndex)
{
 if((newIndex < MMCvComponents[currentMMCtype].length) && (newIndex >= 0) && (newIndex != currentMMCindex)){
  if (!isAAI2Disabled && (currentMMCtype==MMCvVIDEO || currentMMCtype==MMCvAUDIO)) { 
   MMCvComponents[currentMMCtype][currentMMCindex].stopPlayer();
   MMCvComponents[currentMMCtype][currentMMCindex].hideSelf();
   MMCvComponents[currentMMCtype][newIndex].initialize();
   MMCvComponents[currentMMCtype][newIndex].resetCaptionHeight();
   inComponent  = MMCvComponents[currentMMCtype][newIndex].MMCvNode;
   outComponent = MMCvComponents[currentMMCtype][currentMMCindex].MMCvNode;
   MMCvComponents[currentMMCtype][newIndex].showSelf();
   updateMMCvSelectorIndex(currentMMCindex, newIndex);
   currentMMCindex = newIndex; 
   resetContainerHeights(inComponent)
  }
  else if (shiftEnabled)
  {
   MMCvComponents[currentMMCtype][newIndex].initialize();
   MMCvComponents[currentMMCtype][newIndex].resetCaptionHeight();
   shiftEnabled            = 0;
   inComponent             = MMCvComponents[currentMMCtype][newIndex].MMCvNode;
   outComponent            = MMCvComponents[currentMMCtype][currentMMCindex].MMCvNode;
   if (null == inComponent)
   { return; }
   if (null == outComponent)
   { return; }
   if (newIndex < currentMMCindex)
   { inComponent.style.left  = -MMCvWidthComponent; shiftDirection = 1; }
   else
   { inComponent.style.left  = MMCvWidthComponent; shiftDirection = 0; }
   shiftAmountRemaining    = MMCvWidthComponent;
   shiftAmountPerIteration = 25;
   updateMMCvSelectorIndex(currentMMCindex, newIndex);
   currentMMCindex = newIndex;
   if (parseInt(MMCvViewerObject.offsetHeight) < parseInt(inComponent.offsetHeight))
   {
    resetContainerHeights(inComponent) 
   }
   moveComponents();
  }
 }
}

function shiftMMCvRight() 
{ goToMMCvComponent(currentMMCindex+1); }

function shiftMMCvLeft() 
{ goToMMCvComponent(currentMMCindex-1); }

function moveComponents()
{
 if (shiftAmountRemaining > 0)
 {
  if (shiftAmountRemaining < shiftAmountPerIteration)
  { shiftAmountPerIteration = shiftAmountRemaining; }
  if (shiftDirection == 0)
  {
   outComponent.style.left = parseInt(outComponent.style.left) - shiftAmountPerIteration;
   inComponent.style.left = parseInt(inComponent.style.left) - shiftAmountPerIteration;
  }
  else
  {
   outComponent.style.left = parseInt(outComponent.style.left) + shiftAmountPerIteration;
   inComponent.style.left = parseInt(inComponent.style.left) + shiftAmountPerIteration;
  }
  shiftAmountRemaining -= shiftAmountPerIteration;
 }

 if (shiftAmountRemaining > 0)
 {
  shiftEnabled = 0;
  timeoutID1 = setTimeout("moveComponents()", 25);
 }
 else
 {
  clearTimeout(timeoutID1);
  if (parseInt(MMCvViewerObject.offsetHeight) > parseInt(inComponent.offsetHeight))
  { resetContainerHeights(inComponent) }
  shiftEnabled = 1;
 }
}
function getFlashMovie(id) {
 if (navigator.appName.indexOf ("Microsoft") !=-1) {
  return window[id]
 } else {
  return document[id]
 }
}
MMCvComponent.prototype.stopPlayer =  function() { 
 var flashID = this.myID + "WIDGET";
 var flashMovie = getFlashMovie(flashID); 
 if(flashMovie && flashPlayerVersion=='supported'){
  flashMovie.stopPlayBack();
 }
}

function nextArrowOnMouseOver() {
 var nextButton = document.getElementById("nextArrow");
 nextButton.src = '/scidirimg/nextArrowOnHover.gif';
}

function nextArrowOnMouseOut() {
 var nextButton = document.getElementById("nextArrow");
 nextButton.src = '/scidirimg/nextArrowActive.gif';
}

function prevArrowOnMouseOver() {
 var prevButton = document.getElementById("prevArrow");
 prevButton.src = '/scidirimg/prevArrowOnHover.gif';
}

function prevArrowOnMouseOut() {
 var prevButton = document.getElementById("prevArrow");
 prevButton.src = '/scidirimg/prevArrowActive.gif';
}






//-->
